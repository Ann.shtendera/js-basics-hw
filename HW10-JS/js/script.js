"use strict";

const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content')

tabs.addEventListener('click', (event) => {
	if(event.target === event.currentTarget) {
		return;
	}
	if (!event.target.classList.contains('active')) { // якщо повторно тиснемо на активний таб, щоб не перезаписувався клас заново
		tabs.querySelector('.active').classList.remove('active');
		// document.querySelector('.tabs .active').classList.remove('active');
		// Array.from(tabs.children).forEach(tab => {
		// 	if (tab.classList.contains('active')) {
		// 		tab.classList.remove('active')
		// }});
		event.target.classList.add('active')

		// document.querySelector('.tabs-content .shown').classList.remove('shown');
		tabsContent.querySelector('.active').classList.remove('active');

		for (let tabContent of tabsContent.children) {
			if (event.target.dataset.tabs === tabContent.dataset.tabs) {
				tabContent.classList.add('active');
			}
		}
	}
})

