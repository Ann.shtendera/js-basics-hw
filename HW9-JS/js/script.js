"use strict";

function renderList (arr, parent = document.body) {
	const ul = document.createElement('ul');

	arr.forEach(element => {
		const li = document.createElement('li');
		ul.append(li);

		if(Array.isArray(element)) {
			renderList(element, li.previousElementSibling)
			li.remove();
		} else {
			li.innerText = element;
		}
	});
	parent.append(ul);
}

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
renderList(arr)

const arr2 = ["1", "2", "3", "sea", "user", 23];
renderList(arr2, document.body)

const arr3 = ["Kharkiv",["Lozova", "Izium", "Balakliya"], "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
renderList(arr3, document.querySelector('.container'))


const divTimer = document.createElement('div');
divTimer.innerHTML = `<p style="font-size: 20px">Сторінку буде очищенно через 
<span class="timer">3</span> сек.</p>`
document.body.prepend(divTimer)

const timer = document.querySelector('.timer')
let timerId = setInterval( () => {
	timer.innerText -= 1;
	if(timer.innerText === "0") {
		clearInterval(timerId);
		document.body.innerHTML = "";
	}
}, 1000);


