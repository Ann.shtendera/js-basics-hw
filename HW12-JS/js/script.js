'use strict';

// ## Теоретичні питання
// 1. Чому для роботи з input не рекомендується використовувати події клавіатури?

// Справа в тому, що крім клавіатури є інші способи ввести щось в input, наприклад, голосове введення, вставка/копіювання за допомогою миші. Тому події клавіатури не можуть відстежити усі введення. На противагу, є подія input, яка дозволяє відстежувати будь-які зміни в полі input.


const buttons = document.querySelectorAll('.btn');

window.addEventListener("keydown", (e) => {
	const pressedBtn = document.querySelector(".btn.pressed");
	if (pressedBtn) {
		pressedBtn.classList.remove("pressed");
	}
	buttons.forEach((btn) => {
		if (btn.innerText === e.key || btn.innerText.toLowerCase() === e.key) {
			btn.classList.add("pressed");
		}
	});
});