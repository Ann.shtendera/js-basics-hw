"use strict";

function createNewUser() {

    const newUser = {
        firstName: prompt ("Enter your name."),
        lastName: prompt ("Enter your last name."),
		birthday: prompt("Enter your date of birth: `dd.mm.yyyy`", `dd.mm.yyyy`),

        setFirstName(newValue) {
            Object.defineProperty(newUser, "firstName", {value: newValue});
        },
        setLastName(newValue) {
            Object.defineProperty(newUser, "lastName", {value: newValue});
        },

		getAge() {
			const today = new Date();
			const birthDate = new Date(`${this.birthday.slice(-4)}-${this.birthday.slice(3,5)}-${this.birthday.slice(0,2)}`);
	
			const age = today.getFullYear() - birthDate.getFullYear();
	
			if (birthDate.getMonth() > today.getMonth() || 
			birthDate.getMonth() === today.getMonth() && birthDate.getDate() > today.getDate()) {
			return age - 1;
			} 
			return age;
		},
        getLogin () {
     		return (this.firstName[0] + this.lastName).toLowerCase();
        },
		getPassword(){
			return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
		},
    };

    Object.defineProperty(newUser, "firstName", {writable: false});
    Object.defineProperty(newUser, "lastName", {writable: false});

    return newUser;
} 

const user = createNewUser();
console.log(user);
// console.log(user.getLogin());
console.log(`The age of ${user.firstName} ${user.lastName} is:`, user.getAge());
console.log('The password is:', user.getPassword());