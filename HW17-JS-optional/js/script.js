"use strict";

const student = {
	name: prompt("Введіть ваше ім'я:"),
	lastName: prompt("Введіть ваше прізвище:"),
};

let subject, mark;
student.table = {};

do {
	subject = prompt("Введіть назву предмету:");
	if(subject === null) {
		break;
	}
	mark = prompt("Введіть оцінку:");
	if(mark === null) {
		break;
	}
	mark = +mark;

	if (subject && mark) {
		student.table[subject] = mark;
	};
} while (subject !== null && mark !== null);

console.log(student);

let badMarks = 0;
let sum = 0;
let count = 0;

for (let key in student.table) {
	if (student.table[key] < 4) {
		badMarks++;
	}
	sum += student.table[key];
	count++;
};

if (badMarks === 0) {
	console.log(`Студент ${student.name} ${student.lastName} переведено на наступний курс`);
};

const averageMark = sum / count;

if (averageMark > 7) {
	console.log(`Студенту ${student.name} ${student.lastName} призначено стипендію`);
};