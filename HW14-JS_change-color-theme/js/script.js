'use strict';

const themeOrange = document.createElement('link');
themeOrange.rel="stylesheet"
themeOrange.href= "./css/theme-orange.css"

const btnChangeTheme = document.querySelector('.btn-change-theme');

btnChangeTheme.addEventListener('click', (e) => {
	const theme = e.target.dataset.theme;
	changeTheme(theme);
	localStorage.setItem('theme', theme);
});

const changeTheme = (theme) => {
	if (theme === 'orange') { 
		document.head.append(themeOrange); 
		btnChangeTheme.dataset.theme = 'green'; 
	} else {
		btnChangeTheme.dataset.theme = 'orange';
		themeOrange.remove();
	}
}

const currantTheme = localStorage.getItem('theme');

if (!currantTheme) {
	changeTheme('green');
} else {
	changeTheme(currantTheme);
}

