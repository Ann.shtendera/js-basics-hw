'use strict';

const form = document.querySelector('.password-form');

form.addEventListener('click', (e) => {
	const icon = e.target.closest('.icon-password')
	if(!icon) return;
	
	const inputId = icon.dataset.target;
	const inputPass = document.querySelector(inputId);

	if (inputPass.type === 'password') {
		inputPass.type = 'text';
		e.target.classList.replace('fa-eye', 'fa-eye-slash');
	} else {
		inputPass.type = 'password';
		e.target.classList.replace('fa-eye-slash', 'fa-eye');
	}
})


const passwords = form.querySelectorAll('input.password');
const icons = form.querySelectorAll('.icon-password');

const error = document.createElement('span');
error.style.cssText = 'color: red; margin-top: -10px; margin-bottom: 15px'

form.addEventListener('submit', (event) => {
	event.preventDefault();

	if (passwords[0].value === passwords[1].value && passwords[0].value !== "") {
		error.remove()		
		event.target.reset();

		icons.forEach(icon => icon.classList.replace('fa-eye-slash', 'fa-eye'));
		passwords.forEach(input => input.type = 'password');

		setTimeout(() => alert ('Ласкаво просимо!😊'), 100);

	} else if (passwords[0].value === "" && passwords[1].value === "") {
		error.innerText = "Введіть, будь-ласка, пароль."
		passwords[1].closest('.input-wrapper').after(error);

	} else {
		error.innerText = "Потрібно ввести однакові значення!"
		passwords[1].closest('.input-wrapper').after(error);
	}
})