"use strict";

function createNewUser () {

    let userName = prompt ("Enter your name.");
    let userLastName = prompt ("Enter your last name.");

    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin () {
     		return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName(newValue) {
            Object.defineProperty(newUser, "firstName", {value: newValue});
        },
        setLastName(newValue) {
            Object.defineProperty(newUser, "lastName", {value: newValue});
        },
    };

    Object.defineProperty(newUser, "firstName", {writable: false});
    Object.defineProperty(newUser, "lastName", {writable: false});

    return newUser;
} 

const user = createNewUser ();
console.log(user.getLogin());
