"use strict";

// 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let admin;
let name = "Anna";
admin = name;
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

const days = 5;
const seconds = days*24*3600;
console.log(seconds);

// 3. Запитайте у користувача якесь значення і виведіть його в консоль.

const age = prompt ("How old are you?");
console.log(age);