"use strict";

let numberOne = prompt ("Enter the first number.");

while (numberOne === "" || isNaN(numberOne)) {
	numberOne = prompt ("Enter the first number.", numberOne);
}  

let numberTwo = prompt ("Enter the second number.");

while (numberTwo === "" || isNaN(numberTwo)) {
	numberTwo = prompt ("Enter the second number.", numberTwo);
}  

let operation = prompt ('Enter the operation:  " + ", " - ", " * "  or  " / ".');

while (operation || operation === "") {
	if (operation === "+" || operation === "-" || operation === "*" || operation === "/") {
		break;
	} else {
		operation = prompt ('Enter the operation:  " + ", " - ", " * "  or  " / ".', operation);
	}
}

function calcValue(numOne, numTwo, operator) {
	switch (operator) {
		case "+":
			return +numOne + +numTwo;
		case "-":
			return numOne - numTwo;
		case "*":
			return numOne * numTwo;
		case "/":
			return numOne / numTwo;
	}
}

console.log (calcValue (numberOne, numberTwo, operation));