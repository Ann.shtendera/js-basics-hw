"use strict";

const filterBy = (arr, dataType) => arr.filter(element => typeof element !== dataType);

const filteredArray = filterBy(['hello', undefined, ()=>{}, false, 'world', {}, 23, '23', null, []], 'string');

console.log(filteredArray);