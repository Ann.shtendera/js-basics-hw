'use strict';

const btnStop = document.createElement('button');
btnStop.innerText = "Припинити";
const btnRestart = document.createElement('button');
btnRestart.innerText = "Відновити показ";
document.querySelector(".btn-container").append(btnStop, btnRestart);


const images = document.querySelectorAll('.image-to-show');

let slider = 0;
images[0].style.display = 'block';

let isRunning = false;
let timerId;

const createInterval = () => {
	if (isRunning) return timerId;
	isRunning = true;
	
	const timer = setInterval (() => {
		images[slider].style.opacity = 0;

		setTimeout(() => {
			images[slider].style.display = 'none';
			slider++;
			if(slider === images.length) slider = 0;

			images[slider].style.opacity = 0;
			images[slider].style.display = 'block';

			setTimeout(() => {
				images[slider].style.opacity = 1;
			}, 100);
		}, 500);
	}, 3000);

	return timer;
}

timerId = createInterval();

btnStop.addEventListener('click', () => {
	clearInterval(timerId);
	isRunning = false;	
})

btnRestart.addEventListener('click', () => {
	timerId = createInterval();
})

