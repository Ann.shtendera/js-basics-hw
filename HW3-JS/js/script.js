"use strict";

let userNumber = prompt ("Enter a number. The number should be an integer!");

while (userNumber === "" || userNumber % 1 !== 0) {
	userNumber = prompt ("Enter a number. The number should be an integer!");
}

if (userNumber < 5) {
	console.log ("Sorry, no numbers");
} else {
	for (let i = 0; i <= userNumber; i++) {
		if (i % 5 === 0) {
			console.log (i);
		} 
	}
}
