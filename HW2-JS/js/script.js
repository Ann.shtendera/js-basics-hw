"use strict";


let userName = prompt ("Hello! Please, enter your name.");

while (userName === null || userName === "") {
	userName = prompt ("Please, enter your name.");
}

let userAge = prompt ("Enter your age.");

while (userAge === null || userAge === "" || isNaN(userAge)) {
	userAge = prompt ("Enter your age.");
}

if (userAge < 18) {
	alert ("You are not allowed to visit this website");
} else if (userAge >=18 && userAge <= 22) {
	let continueConfirmation = confirm ("Are you sure you want to continue?");
	
	if (continueConfirmation) {
		alert (`Welcome, ${userName}!`);
	} else {
		alert ("You are not allowed to visit this website.");
	}

} else {
	alert (`Welcome, ${userName}!`);
}