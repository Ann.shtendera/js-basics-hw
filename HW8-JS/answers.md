
## Теоретичні питання

1. **Опишіть своїми словами що таке Document Object Model (DOM)**

Document Object Model - об'єктна модель документа, а саме HTML сторінки. Це всі елементи (весь вміст) HTML сторінки у вигляді об'єктів.

2. **Яка різниця між властивостями HTML-елементів innerHTML та innerText?**

innerHTML - властивість для отримання або встановлення всього html-вмісту елемету, включаючи теги, атрибути,текст, переноси строки.
innerText - властивість для отримання або встановлення лише текстового вмісту елементу. 

3. **Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?**

Є декілька способі звернутися до елемента сторінки за допомогою JS:

document.getElementById(id);
document.getElementsByClassName(className);
document.getElementsByTagName(tag);
document.getElementsByName(name);

document.querySelector(css);
document.querySelectorAll(css);

Останні два кращі, бо новіші та більш універсальні.


