"use strict";

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000.

const allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach(el => el.style.background = "#ff0000")


// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

console.log(optionsList.parentElement);

if (optionsList.hasChildNodes()) {
	optionsList.childNodes.forEach(el => {
		console.log(`Type: ${el.nodeType}, name: ${el.nodeName}`);
	})
}


// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

document.querySelector('#testParagraph').innerText = 'This is a paragraph';


// 4) Отримати елементи `<li>` вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const mainHeaderLi = document.querySelectorAll('.main-header li');
console.log(mainHeaderLi);

mainHeaderLi.forEach(el => {
	console.log(el);
	el.classList.add('nav-item');
})


// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  

document.querySelectorAll('.section-title').forEach(el => el.classList.remove('section-title'));