"use strict";

const confirmExit = () => confirm("Are you sure you want to exit?");

const isPositiveInteger = (number) => !!number && !isNaN(number) && number % 1 === 0 && number >= 0;

const getPositiveInteger = (message = "Enter your number.") => {
  let userNumber;

  do {
    userNumber = prompt(message, userNumber ?? "");

    if (userNumber === null) {
      let confirmation = confirmExit();
      if (confirmation) {
        break;
      }
    }
  } while (!isPositiveInteger(userNumber));

  if (userNumber === null) {
    return "cancel";
  }

  return +userNumber;
};

const userNumb = getPositiveInteger("Enter your number. It should be a positive integer.");

const getFactorial = n => (n < 2) ? 1 : n * getFactorial(n - 1);

if (userNumb !== "cancel") {
  const factorial = getFactorial(userNumb);
  console.log(factorial);
}
